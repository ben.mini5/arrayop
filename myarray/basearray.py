# coding: utf-8
from typing import Tuple
from typing import List
from typing import Union

import math


class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type=None, data: Union[Tuple, List]=None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        if not isinstance(shape, (tuple, list)):
            raise Exception(f'shape {shape:} is not a tuple or list')
        for v in shape:
            if not isinstance(v, int):
                raise Exception(f'shape {shape:} contains a non integer {v:}')

        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = int
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n = 1
        for s in self.__shape:
            n *= s

        if data is None:
            if dtype == int:
                self.__data = [0]*n
            elif dtype == float:
                self.__data = [0.0]*n
        else:
            if len(data) != n:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 0 or ind[ax] >= self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds (0, {:})'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if type(inds) is slice:
        inds_itt = range(s)[inds]
    elif type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += inds[n]*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if len(indice) == 0:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True

''' ************************************            MY CODE             ******************************************************  '''

'''
# LOG
def logaritem(a, b):
    if type(a) is BaseArray and type(b) is BaseArray:
        if check_size(a, b) == 1:
            return logArrays(a, b)
        else:
            print("DELITELJ: Nepravilne velikosti matrik !\n")
    elif type(a) is BaseArray:
        return logWithSkalar(a, b)
    elif type(b) is BaseArray:
        return skalarWithLog(a, b)
    else:
        return (math.log(a, b));

def logArrays(a, b):
    c = BaseArray(a.shape, dtype=float)
    if len(a.shape) == 1:
        for i in range(a.shape[0]):
            c.__setitem__(i, math.log(a.__getitem__(i), b.__getitem__(i)))
    elif len(a.shape) == 2:
        for i in range(a.shape[0]):
            for j in range(a.shape[1]):
                c.__setitem__((i,j), math.log(a.__getitem__((i,j)), b.__getitem__((i,j))))
    elif len(a.shape) == 3:
        for h in range(a.shape[2]):
            for i in range(a.shape[0]):
                for j in range(a.shape[1]):
                    c.__setitem__((i,j,h), math.log(a.__getitem__((i,j,h)), b.__getitem__((i,j,h))))
    return c

def logWithSkalar(array, skalar):
    c = BaseArray(array.shape, dtype=float)
    if len(array.shape) == 1:
        for i in range(array.shape[0]):
            c.__setitem__(i, math.log(array.__getitem__(i), skalar))
    elif len(array.shape) == 2:
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                c.__setitem__((i,j), math.log(array.__getitem__((i,j)), skalar))
    elif len(array.shape) == 3:
        for h in range(array.shape[2]):
            for i in range(array.shape[0]):
                for j in range(array.shape[1]):
                    c.__setitem__((i,j,h), math.log(array.__getitem__((i,j,h)), skalar))
    return c

def skalarWithLog(skalar, array):
    c = BaseArray(array.shape, dtype=float)
    if len(array.shape) == 1:
        for i in range(array.shape[0]):
            c.__setitem__(i, math.log(skalar, array.__getitem__(i)))
    elif len(array.shape) == 2:
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                c.__setitem__((i,j), math.log(skalar, array.__getitem__((i,j))))
    elif len(array.shape) == 3:
        for h in range(array.shape[2]):
            for i in range(array.shape[0]):
                for j in range(array.shape[1]):
                    c.__setitem__((i,j,h), math.log(skalar, array.__getitem__((i,j,h))))
    return c 
# POW
def potenca(a, b):
    if type(a) is BaseArray and type(b) is BaseArray:
        if check_size(a, b) == 1:
            return powArrays(a, b)
        else:
            print("DELITELJ: Nepravilne velikosti matrik !\n")
    elif type(a) is BaseArray:
        return powWithSkalar(a, b)
    elif type(b) is BaseArray:
        return skalarWithPow(a, b)
    else:
        return (pow(a, b));

def powArrays(a, b):
    if a.dtype is float or b.dtype is float:
        c = BaseArray(a.shape, dtype=float)  
    else:
        c = BaseArray(a.shape)
    if len(a.shape) == 1:
        for i in range(a.shape[0]):
            c.__setitem__(i, pow(a.__getitem__(i), b.__getitem__(i)))
    elif len(a.shape) == 2:
        for i in range(a.shape[0]):
            for j in range(a.shape[1]):
                c.__setitem__((i,j), pow(a.__getitem__((i,j)), b.__getitem__((i,j))))
    elif len(a.shape) == 3:
        for h in range(a.shape[2]):
            for i in range(a.shape[0]):
                for j in range(a.shape[1]):
                    c.__setitem__((i,j,h), pow(a.__getitem__((i,j,h)), b.__getitem__((i,j,h))))
    return c

def powWithSkalar(array, skalar):
    if type(skalar) is float or array.dtype is float:
        c = BaseArray(array.shape, dtype=float)  
    else:
        c = BaseArray(array.shape)
    if len(array.shape) == 1:
        for i in range(array.shape[0]):
            c.__setitem__(i, pow(array.__getitem__(i), skalar))
    elif len(array.shape) == 2:
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                c.__setitem__((i,j), pow(array.__getitem__((i,j)), skalar))
    elif len(array.shape) == 3:
        for h in range(array.shape[2]):
            for i in range(array.shape[0]):
                for j in range(array.shape[1]):
                    c.__setitem__((i,j,h), pow(array.__getitem__((i,j,h)), skalar))
    return c

def skalarWithPow(skalar, array):
    if type(skalar) is float or array.dtype is float:
        c = BaseArray(array.shape, dtype=float)  
    else:
        c = BaseArray(array.shape)
    if len(array.shape) == 1:
        for i in range(array.shape[0]):
            c.__setitem__(i, pow(skalar, array.__getitem__(i)))
    elif len(array.shape) == 2:
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                c.__setitem__((i,j), pow(skalar, array.__getitem__((i,j))))
    elif len(array.shape) == 3:
        for h in range(array.shape[2]):
            for i in range(array.shape[0]):
                for j in range(array.shape[1]):
                    c.__setitem__((i,j,h), pow(skalar, array.__getitem__((i,j,h))))
    return c 
'''

def izpis(array):
    if type(array) is BaseArray:
        k = 0
        if len(array.shape) == 1:
            for i in range(array.shape[0]):
                if array._BaseArray__data[k] < 0:
                    if array.dtype is float:
                        print("{0:.2f}".format(array._BaseArray__data[k]), " ", end="", flush=True)
                    else:
                        print(array._BaseArray__data[k], " ", end="", flush=True)
                else:
                    if array.dtype is float:
                        print("", "{0:.2f}".format(array._BaseArray__data[k]), " ", end="", flush=True)
                    else:
                        print("", array._BaseArray__data[k], " ", end="", flush=True)
                k = k + 1
            print()
        elif len(array.shape) == 2:
            for i in range(array.shape[0]):
                for j in range(array.shape[1]):
                    if array._BaseArray__data[k] < 0:
                        if array.dtype is float:
                            print("{0:.2f}".format(array._BaseArray__data[k]), " ", end="", flush=True)
                        else:
                            print(array._BaseArray__data[k], " ", end="", flush=True)
                    else:
                        if array.dtype is float:
                            print("", "{0:.2f}".format(array._BaseArray__data[k]), " ", end="", flush=True)
                        else:
                            print("", array._BaseArray__data[k], " ", end="", flush=True)
                    k = k + 1
                print()
        elif len(array.shape) == 3:
            for h in range(array.shape[2]):
                for i in range(array.shape[0]):
                    for j in range(array.shape[1]):
                        if array._BaseArray__data[k] < 0:
                            if array.dtype is float:
                                print("{0:.2f}".format(array._BaseArray__data[k]), " ", end="", flush=True)
                            else:
                                print(array._BaseArray__data[k], " ", end="", flush=True)
                        else:
                            if array.dtype is float:
                                print("", "{0:.2f}".format(array._BaseArray__data[k]), " ", end="", flush=True)
                            else:
                                print("", array._BaseArray__data[k], " ", end="", flush=True)
                        k = k + 1
                    print()
                print()        
        else:
            raise Exception("izpis(): Function for matrice of shape: {0}, not defined".format(array.shape))
    else:
        print(array)    
    print()

def merge_sort(array):
    if len(array)>1:
        mid = len(array)//2
        left = array[:mid]
        right = array[mid:]
        merge_sort(left)
        merge_sort(right)

        i=0
        j=0
        k=0
        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                array[k]=left[i]
                i=i+1
            else:
                array[k]=right[j]
                j=j+1
            k=k+1

        while i < len(left):
            array[k]=left[i]
            i=i+1
            k=k+1

        while j < len(right):
            array[k]=right[j]
            j=j+1
            k=k+1

def sort(array, argument): 
    if len(array.shape) == 1:
            copy = []
            k = 0
            for i in range(array.shape[0]):
                copy.append(array.__getitem__(i))
            merge_sort(copy)
            for i in range(array.shape[0]):
                 array.__setitem__(i, copy[i])
    else:
        if argument == "v":
            for j in range(array.shape[0]):
                copy = []
                for i in range(array.shape[1]):
                    copy.append(array.__getitem__((j, i)))
                merge_sort(copy)
                for i in range(array.shape[1]):
                    array.__setitem__((j, i), copy[i])
        elif argument == "s":
            for j in range(array.shape[1]):
                copy = []
                for i in range(array.shape[0]):
                    copy.append(array.__getitem__((i, j)))
                merge_sort(copy)
                for i in range(array.shape[0]):
                    array.__setitem__((i, j), copy[i])
        else:
            raise Exception("sort(): No definition for argument: {0}".format(argument))
    return array

def iskalnik(array, num):   
    k = 0
    counter = 0
    if len(array.shape) == 1:
        for i in range(array.shape[0]):
            if array.__getitem__(i) == num:
                print("(", i, ")")
                coutner = counter + 1
    elif len(array.shape) == 2:
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                if array.__getitem__((i, j)) == num:
                    print("(", i, ",", j, ")")
                coutner = counter + 1
    elif len(array.shape) == 3:
        for h in range(array.shape[2]):
            for i in range(array.shape[0]):
                for j in range(array.shape[1]):
                    if array.__getitem__((i, j, h)) == num:
                        print("(", i, ",", j, ",", h, ")")
                coutner = counter + 1
    else:
        raise Exception("izpis(): Function for matrice of shape: {0}, not defined".format(array.shape))
    if counter < 1:
        print("The matrice does not contain the number: {0}").format(num)
                    

def check_size(a, b):
    if len(a.shape) == len(b.shape):
        for i in range(len(a.shape)):
            if(a.shape[i] != b.shape[i]):
                return 0
        return 1
    else:
        return 0
    return 1

# ADD
def sestevalnik(a, b):
    if type(a) is BaseArray and type(b) is BaseArray:
        if check_size(a, b) == 1:
            return addArrays(a, b)
        else:
            raise Exception("sestevalnik(): Matrtice shapes do not match")
    elif type(a) is BaseArray:
        return addWithSkalar(a, b)
    elif type(b) is BaseArray:
        return addWithSkalar(b, a)
    else:
        return (a + b);
    
def addArrays(a, b):
    if a.dtype is float or b.dtype is float:
        c = BaseArray(a.shape, dtype=float)  
    else:
        c = BaseArray(a.shape)
    if len(a.shape) == 1:
        for i in range(a.shape[0]):
            c.__setitem__(i, (a.__getitem__(i) + b.__getitem__(i)))
    elif len(a.shape) == 2:
        for i in range(a.shape[0]):
            for j in range(a.shape[1]):
                c.__setitem__((i,j), (a.__getitem__((i,j)) + b.__getitem__((i,j))))
    elif len(a.shape) == 3:
        for h in range(a.shape[2]):
            for i in range(a.shape[0]):
                for j in range(a.shape[1]):
                    c.__setitem__((i,j,h), (a.__getitem__((i,j,h)) + b.__getitem__((i,j,h))))
    return c

def addWithSkalar(array, skalar):
    if type(skalar) is float or array.dtype is float:
        c = BaseArray(array.shape, dtype=float)  
    else:
        c = BaseArray(array.shape)
    if len(array.shape) == 1:
        for i in range(array.shape[0]):
            c.__setitem__(i, (array.__getitem__(i) + skalar))
    elif len(array.shape) == 2:
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                c.__setitem__((i,j), (array.__getitem__((i,j)) + skalar))
    elif len(array.shape) == 3:
        for h in range(array.shape[2]):
            for i in range(array.shape[0]):
                for j in range(array.shape[1]):
                    c.__setitem__((i,j,h), (array.__getitem__((i,j,h)) + skalar))
    return c
# SUB
def odstevalnik(a, b):
    if type(a) is BaseArray and type(b) is BaseArray:
        if check_size(a, b) == 1:
            return subtractArrays(a, b)
        else:
            raise Exception("odstevalnik(): Matrtice shapes do not match")
    elif type(a) is BaseArray:
        return subtractWithSkalar(a, b)
    elif type(b) is BaseArray:
        return skalarWithSubstract(a, b)
    else:
        return (a - b);

def subtractArrays(a, b):
    if a.dtype is float or b.dtype is float:
        c = BaseArray(a.shape, dtype=float)  
    else:
        c = BaseArray(a.shape)
    if len(a.shape) == 1:
        for i in range(a.shape[0]):
            c.__setitem__(i, (a.__getitem__(i) - b.__getitem__(i)))
    elif len(a.shape) == 2:
        for i in range(a.shape[0]):
            for j in range(a.shape[1]):
                c.__setitem__((i,j), (a.__getitem__((i,j)) - b.__getitem__((i,j))))
    elif len(a.shape) == 3:
        for h in range(a.shape[2]):
            for i in range(a.shape[0]):
                for j in range(a.shape[1]):
                    c.__setitem__((i,j,h), (a.__getitem__((i,j,h)) - b.__getitem__((i,j,h))))
    return c

def subtractWithSkalar(array, skalar):
    if type(skalar) is float or array.dtype is float:
        c = BaseArray(array.shape, dtype=float)  
    else:
        c = BaseArray(array.shape)
    if len(array.shape) == 1:
        for i in range(array.shape[0]):
            c.__setitem__(i, (array.__getitem__(i) - skalar))
    elif len(array.shape) == 2:
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                c.__setitem__((i,j), (array.__getitem__((i,j)) - skalar))
    elif len(array.shape) == 3:
        for h in range(array.shape[2]):
            for i in range(array.shape[0]):
                for j in range(array.shape[1]):
                    c.__setitem__((i,j,h), (array.__getitem__((i,j,h)) - skalar))
    return c

def skalarWithSubstract(skalar, array):
    if type(skalar) is float or array.dtype is float:
        c = BaseArray(array.shape, dtype=float)  
    else:
        c = BaseArray(array.shape)
    if len(array.shape) == 1:
        for i in range(array.shape[0]):
            c.__setitem__(i, (skalar - array.__getitem__(i)))
    elif len(array.shape) == 2:
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                c.__setitem__((i,j), (skalar - array.__getitem__((i,j))))
    elif len(array.shape) == 3:
        for h in range(array.shape[2]):
            for i in range(array.shape[0]):
                for j in range(array.shape[1]):
                    c.__setitem__((i,j,h), (skalar - array.__getitem__((i,j,h))))
    return c
# MUL
def mnozilec(a, b):
    if type(a) is BaseArray and type(b) is BaseArray:
        if a.shape[1] == b.shape[0]:
            return mulitplyArrays(a, b)
        else:
            raise Exception("mnozilec(): Matrtice shapes do not match")
    elif type(a) is BaseArray:
        return multiplyWithSkalar(a, b)
    elif type(b) is BaseArray:
        return multiplyWithSkalar(b, a)
    else:
        return (a * b);

def mulitplyArrays(a, b):
    if a.dtype is float or b.dtype is float:
        c = BaseArray((a.shape[0], b.shape[1]), dtype=float)  
    else:
        c = BaseArray((a.shape[0], b.shape[1]))
    c = BaseArray(a.shape);
    for i in range(a.shape[0]):
        for j in range(b.shape[1]):
            for k in range(b.shape[0]):
                c.__setitem__((i,j), (c.__getitem__((i,j)) + (a.__getitem__((i,k)) * b.__getitem__((k,j)) ) ) )
    return c

def multiplyWithSkalar(array, skalar):
    if type(skalar) is float or array.dtype is float:
        c = BaseArray(array.shape, dtype=float)  
    else:
        c = BaseArray(array.shape)
    if len(array.shape) == 1:
        for i in range(array.shape[0]):
            c.__setitem__(i, (array.__getitem__(i) * skalar))
    elif len(array.shape) == 2:
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                c.__setitem__((i,j), (array.__getitem__((i,j)) * skalar))
    elif len(array.shape) == 3:
        for h in range(array.shape[2]):
            for i in range(array.shape[0]):
                for j in range(array.shape[1]):
                    c.__setitem__((i,j,h), (array.__getitem__((i,j,h)) * skalar))
    return c
# DIV
def delitelj(a, b):
    if type(a) is BaseArray and type(b) is BaseArray:
        if check_size(a, b) == 1:
            return divideArrays(a, b)
        else:
            raise Exception("delitelj(): Matrtice shapes do not match")
    elif type(a) is BaseArray:
        return divideWithSkalar(a, b)
    elif type(b) is BaseArray:
        return skalarWithDivide(a, b)
    else:
        return (a / b);

def divideArrays(a, b):
    c = BaseArray(a.shape, dtype=float)
    if len(a.shape) == 1:
        for i in range(a.shape[0]):
            c.__setitem__(i, (a.__getitem__(i) / b.__getitem__(i)))
    elif len(a.shape) == 2:
        for i in range(a.shape[0]):
            for j in range(a.shape[1]):
                c.__setitem__((i,j), (a.__getitem__((i,j)) / b.__getitem__((i,j))))
    elif len(a.shape) == 3:
        for h in range(a.shape[2]):
            for i in range(a.shape[0]):
                for j in range(a.shape[1]):
                    c.__setitem__((i,j,h), (a.__getitem__((i,j,h)) / b.__getitem__((i,j,h))))
    return c

def divideWithSkalar(array, skalar):
    c = BaseArray(array.shape, dtype=float)
    if len(array.shape) == 1:
        for i in range(array.shape[0]):
            c.__setitem__(i, (array.__getitem__(i) / skalar))
    elif len(array.shape) == 2:
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                c.__setitem__((i,j), (array.__getitem__((i,j)) / skalar))
    elif len(array.shape) == 3:
        for h in range(array.shape[2]):
            for i in range(array.shape[0]):
                for j in range(array.shape[1]):
                    c.__setitem__((i,j,h), (array.__getitem__((i,j,h)) / skalar))
    return c

def skalarWithDivide(skalar, array):
    c = BaseArray(array.shape, dtype=float)
    if len(array.shape) == 1:
        for i in range(array.shape[0]):
            c.__setitem__(i, (skalar / array.__getitem__(i)))
    elif len(array.shape) == 2:
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                c.__setitem__((i,j), (skalar / array.__getitem__((i,j))))
    elif len(array.shape) == 3:
        for h in range(array.shape[2]):
            for i in range(array.shape[0]):
                for j in range(array.shape[1]):
                    c.__setitem__((i,j,h), (skalar / array.__getitem__((i,j,h))))
    return c
# LOG
def logaritem(array):
    c = BaseArray(array.shape, dtype=float)
    if len(array.shape) == 1:
        for i in range(array.shape[0]):
            c.__setitem__(i,  math.log(array.__getitem__(i)))
    elif len(array.shape) == 2:
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                c.__setitem__((i,j),  math.log(array.__getitem__((i,j))))
    elif len(array.shape) == 3:
        for h in range(array.shape[2]):
            for i in range(array.shape[0]):
                for j in range(array.shape[1]):
                    c.__setitem__((i,j,h), math.log(array.__getitem__((i,j,h))))
    return c
# EXP
def potenca(array):
    c = BaseArray(array.shape, dtype=float)
    if len(array.shape) == 1:
        for i in range(array.shape[0]):
            c.__setitem__(i,  math.exp(array.__getitem__(i)))
    elif len(array.shape) == 2:
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                c.__setitem__((i,j),  math.exp(array.__getitem__((i,j))))
    elif len(array.shape) == 3:
        for h in range(array.shape[2]):
            for i in range(array.shape[0]):
                for j in range(array.shape[1]):
                    c.__setitem__((i,j,h), math.exp(array.__getitem__((i,j,h))))
    return c


# **********************************************            PREVERJANJE            *********************************************** #    

def test_isEuqal(a, b):
    if len(a.shape) == 1:
        for i in range(a.shape[0]):
            if a.__getitem__(i) != b.__getitem__(i):
                return 0
    elif len(a.shape) == 2:
        for i in range(a.shape[0]):
            for j in range(a.shape[1]):
                if a.__getitem__((i,j)) != b.__getitem__((i,j)):
                    return 0
    elif len(a.shape) == 3:
        for h in range(a.shape[2]):
            for i in range(a.shape[0]):
                for j in range(a.shape[1]):
                    if a.__getitem__((i,j,h)) != b.__getitem__((i,j,h)):
                        return 0
    else :
        if a != b:
            return 0
    return 1


def test_izpis():
    a = BaseArray((3, 3), data=range(9))
    print("2D izpis: \n")
    izpis(a)
    b = BaseArray((3, 3, 3), data=range(27))
    print("3D izpis: \n")
    izpis(b)

def test_sort():
    a = BaseArray((3, 3), data=(8,7,6,5,4,3,2,1,0))
    V = sort(a, "v")
    a = BaseArray((3, 3), data=(8,7,6,5,4,3,2,1,0))
    S = sort(a, "s")
    testV = BaseArray((3, 3), data=(6,7,8,3,4,5,0,1,2))
    testS = BaseArray((3, 3), data=(2,1,0,5,4,3,8,7,6))
    if test_isEuqal(V, testV) != 1:
        print("Row sorting failed\n")
    if test_isEuqal(S, testS) != 1:
        print("Column sorting failed\n")
    testB = BaseArray((9,), data=(0,1,2,3,4,5,6,7,8))
    b = BaseArray((9,), data=(8,7,6,5,4,3,2,1,0))
    b = sort(b, "a")
    if test_isEuqal(b, testB) != 1:
        print("1D sorting failed\n")

test_sort()

def test_iskalnik():
    return 0

def test_sestevalnik():
    a2 = BaseArray((3, 3), data=(8,7,6,5,4,3,2,1,0))
    b2 = BaseArray((3, 3), data=(0,1,2,3,4,5,6,7,8))
    c2 = sestevalnik(a2, b2)
    test2 = BaseArray((3,3), data=(8,8,8,8,8,8,8,8,8))
    if test_isEuqal(c2, test2) != 1:
        print("2D addition failed")
    a3 = BaseArray((2, 2, 2), data=(7,6,5,4,3,2,1,0))
    b3 = BaseArray((2, 2, 2), data=(0,1,2,3,4,5,6,7))
    c3 = sestevalnik(a3, b3)
    test3 = BaseArray((2, 2, 2), data=(7,7,7,7,7,7,7,7))
    if test_isEuqal(c3, test3) != 1:
        print("3D addition failed")

test_sestevalnik

def test_odstevalnik():
    a2 = BaseArray((3, 3), data=(8,7,6,5,4,3,2,1,0))
    b2 = BaseArray((3, 3), data=(0,1,2,3,4,5,6,7,8))
    c2 = odstevalnik(a2, b2)
    test2 = BaseArray((3,3), data=(8,6,4,2,0,-2,-4,-6,-8))
    if test_isEuqal(c2, test2) != 1:
        print("2D substraction failed")
    a3 = BaseArray((2, 2, 2), data=(7,6,5,4,3,2,1,0))
    b3 = BaseArray((2, 2, 2), data=(0,1,2,3,4,5,6,7))
    c3 = odstevalnik(a3, b3)
    test3 = BaseArray((2, 2, 2), data=(7,5,3,1,-1,-3,-5,-7))
    if test_isEuqal(c3, test3) != 1:
        print("3D substraction failed")

test_odstevalnik()

def test_mnozilec():
    a2 = BaseArray((3, 3), data=(8,7,6,5,4,3,2,1,0))
    b2 = BaseArray((3, 3), data=(0,1,2,3,4,5,6,7,8))
    c2 = mnozilec(a2, b2)
    test2 = BaseArray((3,3), data=(57,78,99,30,42,54,3,6,9))
    if test_isEuqal(c2, test2) != 1:
        print("2D multiplication failed")
    a3 = BaseArray((2, 2, 2), data=(7,6,5,4,3,2,1,0))
    b3 = 3
    c3 = mnozilec(a3, b3)
    test3 = BaseArray((2, 2, 2), data=(21,18,15,12,9,6,3,0))
    if test_isEuqal(c3, test3) != 1:
        print("3D skalasr multiplication failed")

test_mnozilec()

def test_delitelj():
    a2 = BaseArray((3, 3), data=(2,4,6,8,10,12,14,16,18))
    b2 = BaseArray((3,3), data=(2,2,2,2,2,2,2,2,2))
    c2 = delitelj(a2, b2)
    test2 = BaseArray((3, 3), data=(1,2,3,4,5,6,7,8,9))
    if test_isEuqal(c2, test2) != 1:
        print("2D divison failed")
    a3 = BaseArray((2, 2, 2), data=(21,18,15,12,9,6,3,0))
    b3 = 3
    c3 = delitelj(a3, b3)
    test3 = BaseArray((2, 2, 2), data=(7,6,5,4,3,2,1,0))
    if test_isEuqal(c3, test3) != 1:
        print("3D skalar divison failed")

test_delitelj()

'''

array = BaseArray((5,), data=(0, 5, 4, 0, 3))
c = BaseArray((3, 3), data=(8,7,6,5,4,3,2,1,0))
d = BaseArray((2, 2, 2), data=(range(8)))
e = BaseArray((2, 3, 3), data=(range(18)))
f = BaseArray((2, 4), data=(range(8)))
g = BaseArray((2, 4), data=(2,1,2,1,2,1,2,1))
x = BaseArray((3, 3), data=(34, 1, 77, 2, 14, 8, 3, 17, 11));
y = BaseArray((3, 3), data=(6, 8, 1, 9, 27, 5, 2, 43, 31));
r = BaseArray((2, 2, 2, 2), data=(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1));
'''

'''
izpis(f)
izpis(logaritem(g))
izpis(potenca(f))

izpis(a)
'''

'''
iskalnik(a, 0)
iskalnik(b, 0)

izpis(e)
iskalnik(e, 13)

izpis(d)
iskalnik(d, 5)
'''
'''
izpis(f)
izpis(g)
izpis(mnozilec(g, 3))
'''
